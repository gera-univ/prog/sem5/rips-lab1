package org.spiralarms.ripslab1;

/**
 * Represents a car
 * Car contains one wheel, has limited fuel and mileage that increases as car rides
 * Car can refuel, have its wheels replaced and perform a ride
 */
public class Car {
    private CarModel model;
    private double fuel;
    private double mileage;
    private Wheels wheels;

    public Car(CarModel model, double fuel, double mileage, double wheelHealth, double wheelWear) {
        this.model = model;
        this.fuel = fuel;
        this.mileage = mileage;
        this.wheels = new Wheels(wheelHealth, wheelWear);
    }

    public Car(CarModel model, Wheels wheel) {
        this.wheels = wheel;
        this.model = model;
    }

    /**
     * Cap car's fuel to tank size
     */
    public void refuel() {
        fuel = model.getTankSize();
    }

    public CarModel getModel() {
        return model;
    }

    public double getFuel() {
        return fuel;
    }

    public void consumeFuel(double amount) {
        fuel -= amount;
    }

    public double getMileage() {
        return mileage;
    }

    public void addMileage(double km) {
        mileage += km;
    }

    public Wheels getWheels() {
        return wheels;
    }

    public void replaceWheels(Wheels wheel) {
        this.wheels = wheel;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model=" + model +
                ", fuel=" + fuel +
                ", mileage=" + mileage +
                ", wheel=" + wheels +
                '}';
    }
}

package org.spiralarms.ripslab1;

/**
 * Controls the main application
 */
public class Controller {
    View view;
    Car car;

    public Controller(View view) {
        this.view = view;
        CarModel model = view.askCarModelParameters();
        Wheels wheels = view.askWheelsParameters();
        car = new Car(model, wheels);
    }

    public void actionRefuel() {
        car.refuel();
    }

    public void actionReplaceWheels() {
        Wheels w = view.askWheelsParameters();
        car.replaceWheels(w);
    }

    public void actionPrintCarInformation() {
        view.printCarInformation(car);
    }

    /**
     * Travels the specified distance or until wheel wears out or car is out of fuel
     * Travelled distance contributes to the car's milleage
     *
     * @param km How far to travel
     * @throws WheelWornOutException
     * @throws OutOfFuelException
     */
    public void driveCar(double km) throws WheelWornOutException, OutOfFuelException {
        double fuelRequired = km * car.getModel().getFuelConsumtion();
        double fuel = car.getFuel();
        if (fuel < fuelRequired) {
            km = fuel / car.getModel().getFuelConsumtion();
            try {
                wearWheels(car.getWheels(), km);
                car.consumeFuel(fuel);
            } catch (WheelWornOutException e) {
                car.consumeFuel(km * car.getModel().getFuelConsumtion());
                throw e;
            } finally {
                car.addMileage(km);
            }
            throw new OutOfFuelException();
        } else {
            try {
                wearWheels(car.getWheels(), km);
                fuel -= fuelRequired;
            } catch (WheelWornOutException e) {
                car.consumeFuel(km * car.getModel().getFuelConsumtion());
                throw e;
            } finally {
                car.addMileage(km);
            }
        }
    }

    /**
     * Travels the specified distance or until wheel wears out or car is out of fuel
     *
     * @param km How far to travel
     * @throws WheelWornOutException
     * @throws OutOfFuelException
     */
    public void wearWheels(Wheels w, double km) throws WheelWornOutException, OutOfFuelException {
        double healthRequired = km * w.getWear();
        if (w.getHealth() < healthRequired) {
            w.decreaseHealth(w.getHealth());
            throw new WheelWornOutException();
        } else {
            w.decreaseHealth(healthRequired);
        }
    }

    public void actionTestDrive() {
        double distance = view.askTestDriveDistance();
        try {
            driveCar(distance);
        } catch (WheelWornOutException e) {
            view.showWheelWornOutMessage();
        } catch (OutOfFuelException e) {
            view.showOutOfFuelMessage();
        }
    }
}
